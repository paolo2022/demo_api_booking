package apis.updatebooking;

import com.intuit.karate.junit5.Karate;

public class UpdateBookingRunner {
    @Karate.Test
    Karate testBooking() {
        return Karate.run("createbooking").relativeTo(getClass());
    }
}
