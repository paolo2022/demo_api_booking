Feature: Api booking
  
  Background:

    * url url
    * header Content-Type = 'application/json'
    * header Accept = 'application/json'
    * def createToken = call read('classpath:apis/createtoken/createtoken.feature')
    * cookie token = createToken.response.token

    Scenario: Update booking
      * def body =
      """
      {
       "firstname" : "paolo",
        "lastname" : "criales",
       "totalprice" : 250,
        "depositpaid" : false,
       "bookingdates" : {
            "checkin" : "2022-01-01",
            "checkout" : "2023-01-01"
    },
      "additionalneeds" : "QA Automation"
    }
      """
      * def id = 1
      Given path 'booking' , id
      And request body
      When method put
      Then status 200


  Scenario: Patch Update booking
    * def body =
      """
      {
       "firstname" : "steve",
        "lastname" : "salinas",
       "totalprice" : 250,

    }
      """
    * def id = 1
    Given path 'booking' , id
    And request body
    When method patch
    Then status 200