package apis.createtoken;

import com.intuit.karate.junit5.Karate;

public class CreateTokenRunner {
    @Karate.Test
    Karate testBooking() {
        return Karate.run("createtoken").relativeTo(getClass());
    }
}
