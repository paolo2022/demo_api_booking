Feature: Api Booking

  Background:
    * url url
    * header Accept = 'application/json'


    Scenario: Method get all booking
      Given path 'booking'
      When method get
      Then status 200
      * def count = response.length
      * print 'Nro de registros --->', count
     # * match count == 24

  Scenario: get details booking
   * def id = 1
    Given path 'booking', id
    When method get
    Then status 200
#    And match response.totalprice == 635
#    And match response.firstname == 'Eric'
    * print response.totalprice

   Scenario Outline: Get details booking with datatable <id>
     Given path 'booking', <id>
     When method get
     Then status 200
     Examples:
     |id|
     |1 |
     |2 |
     |3 |
     |4 |

  Scenario Outline: Get details booking with datatable CSV <id>
    Given path 'booking', <id>
    When method get
    Then status 200
    Examples:
      |read('data.csv')|