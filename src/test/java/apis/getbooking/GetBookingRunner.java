package apis.getbooking;

import com.intuit.karate.junit5.Karate;

public class GetBookingRunner {
    @Karate.Test
    Karate testBooking() {
        return Karate.run("getbooking").relativeTo(getClass());
    }
}
