Feature: Api Booking

  Background:
    * url url
    * header Accept = 'application/json'
    * header Content-Type = 'application/json'

  Scenario: Crear booking
    * def body =
      """
      {
       "firstname" : "Jim",
        "lastname" : "Brown",
       "totalprice" : 111,
        "depositpaid" : true,
       "bookingdates" : {
            "checkin" : "2018-01-01",
            "checkout" : "2019-01-01"
    },
      "additionalneeds" : "Breakfast"
    }
      """
    Given path 'booking'
    And request body
    When  method post
    Then status 200
    And match response.booking.firstname contains 'Jim'
    And match response.booking.lastname contains 'Brown'
    And match response.booking.totalprice contains 111
    And match response.booking.depositpaid contains true
    And match response.booking.bookingdates.checkin contains '2018-01-01'
    And match response.booking.bookingdates.checkout contains '2019-01-01'
    And match response.booking.additionalneeds contains 'Breakfast'

    Scenario Outline: Crear booking datatable
    * def body =
      """
      {
       "firstname" : "<firstname>",
        "lastname" : "<lastname>",
       "totalprice" : <totalprice>,
        "depositpaid" : <depositpaid>,
       "bookingdates" : {
            "checkin" : "<checkin>",
            "checkout" : "<checkout>"
    },
      "additionalneeds" : "<additionalneeds>"
    }
      """
    Given path 'booking'
    And request body
    When  method post
    Then status 200
    And match response.booking.firstname contains '<firstname>'
    And match response.booking.lastname contains '<lastname>'
    And match response.booking.totalprice contains <totalprice>
    And match response.booking.depositpaid contains <depositpaid>
    And match response.booking.bookingdates.checkin contains '<checkin>'
    And match response.booking.bookingdates.checkout contains '<checkout>'
    And match response.booking.additionalneeds contains '<additionalneeds>'

    Examples:
      | firstname | lastname | totalprice | depositpaid | checkin    | checkout    | additionalneeds |
      | paolo     | criales  | 150        | false       | 2020-01-01 | 20201-01-01 | prueba 1        |
      | pamela    | criales  | 178        | true        | 2020-01-01 | 20201-01-01 | prueba 2        |
      | augusto   | criales  | 130        | false       | 2020-01-01 | 20201-01-01 | prueba 3        |
      | steve     | criales  | 145        | true        | 2020-01-01 | 20201-01-01 | prueba 4        |


  Scenario: Crear booking with JSON
    * def data = read('classpath:src/test/java/apis/createbooking/data.json')
    * def body =
      """
      {
       "firstname" : "#(data.firstname)",
        "lastname" : "#(data.lastname)",
       "totalprice" : #(data.totalprice),
        "depositpaid" : #(data.depositpaid),
       "bookingdates" : {
            "checkin" : "#(data.bookingdates.checkin)",
            "checkout" : "#(data.bookingdates.checkout)"
    },
      "additionalneeds" : "#(data.additionalneeds)"
    }
      """
    Given path 'booking'
    And request body
    When  method post
    Then status 200
    And match response.booking.firstname contains data.firstname
    And match response.booking.lastname contains data.lastname
    And match response.booking.totalprice contains data.totalprice
    And match response.booking.depositpaid contains data.depositpaid
    And match response.booking.bookingdates.checkin contains data.bookingdates.checkin
    And match response.booking.bookingdates.checkout contains data.bookingdates.checkout
    And match response.booking.additionalneeds contains data.additionalneeds

  Scenario Outline: Crear booking with JSON 2
    * def body =
      """
      {
       "firstname" : "#(firstname)",
        "lastname" : "#(lastname)",
       "totalprice" : #(totalprice),
        "depositpaid" : #(depositpaid),
       "bookingdates" : {
            "checkin" : "#(bookingdates.checkin)",
            "checkout" : "#(bookingdates.checkout)"
    },
      "additionalneeds" : "#(additionalneeds)"
    }
      """
    Given path 'booking'
    And request body
    When  method post
    Then status 200
    And match response.booking.firstname contains firstname
    And match response.booking.lastname contains lastname
    And match response.booking.totalprice contains totalprice
    And match response.booking.depositpaid contains depositpaid
    And match response.booking.bookingdates.checkin contains bookingdates.checkin
    And match response.booking.bookingdates.checkout contains bookingdates.checkout
    And match response.booking.additionalneeds contains additionalneeds
    Examples:
    |read('data1.json')|


  Scenario: Validate Schema Create Booking
    * string data = read('classpath:src/test/java/apis/createbooking/schema-create-booking.json')
    * def validateSchema = Java.type('util.schemaUtils')
    * def body =
      """
      {
       "firstname" : "Jim",
        "lastname" : "Brown",
       "totalprice" : 111,
        "depositpaid" : true,
       "bookingdates" : {
            "checkin" : "2018-01-01",
            "checkout" : "2019-01-01"
    },
      "additionalneeds" : "Breakfast"
    }
      """
    Given path 'booking'
    And request body
    When  method post
    Then status 200
    And assert validateSchema.isSchemaValid(response, data)