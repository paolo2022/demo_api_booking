package apis.createbooking;

import com.intuit.karate.junit5.Karate;

public class CreateBookingRunner {
    @Karate.Test
    Karate testBooking() {
        return Karate.run("createbooking").relativeTo(getClass());
    }
}
